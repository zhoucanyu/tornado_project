import tornado.web
from db.mysql import DbMysql
from url import urls
import redis


class App(tornado.web.Application):
    def __init__(self, options):

        super(App, self).__init__(
            handlers=urls,
            **options['settings']
        )

        self.mysql = DbMysql(**options['mysql'])

        self.redis = redis.StrictRedis(**options['redis'])
