
from extend import helper
from .dev import dev_config
from .prod import prod_config
import os


BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
default = {
    'author': 'cy',
    'version': '1.0',
    'port': 9000,
    'title': 'my-tornado-project',
    'log_file': os.path.join(BASE_DIR, 'logs/log')
}


def init_config(environ):
    if environ == 'dev':
        return dev_config
    if environ == 'prod':
        return helper.deepSearch(dev_config, prod_config)
