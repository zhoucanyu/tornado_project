import os


BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
settings = {
    'debug': True,
    "autoreload": True,
    'cookie_secret': "123456",
    'login_url': "/login",
    'template_path': os.path.join(BASE_DIR, 'templates'),
    'static_path': os.path.join(BASE_DIR, 'static'),
    "compiled_template_cache": False,
    "static_hash_cache": False,
    "server_traceback": True,
    "cookie_secert": 'tornado',
    "xsrf_cookies": False
}
mysql = {
    "host": '106.15.126.81',
    "user": 'view_ipdatabase',
    "passwd": 'view_ipdatabase+1s',
    "port": 8999,
    "dbName": 'TongJiDev',
    "charset": 'utf8mb4'
}
redis = {
    "host": '119.23.219.245',
    "port": '8000',
    "password": '4%CPpOoUPML0&SMa',
    "db": '9',
}

dev_config = dict(
    settings=settings,
    mysql=mysql,
    redis=redis
)
