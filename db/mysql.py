import pymysql


class DbMysql():
    def __init__(self, *args, **kwargs):
        self.host = kwargs['host']
        self.port = kwargs['port']
        self.user = kwargs['user']
        self.passwd = kwargs['passwd']
        self.dbName = kwargs['dbName']
        self.charset = kwargs['charset']
        self.connet()

    def connet(self):
        db = pymysql.connect(
            host=self.host,
            user=self.user,
            passwd=self.passwd,
            db=self.dbName,
            port=self.port,
            charset=self.charset
        )
        self.db = db
        self.cursor = db.cursor(cursor=pymysql.cursors.DictCursor)

    def findAll(self, sql):
        self.cursor.execute(sql)
        lines = self.cursor.fetchall()
        return lines

    def findOne(self, sql):
        self.cursor.execute(sql)
        lines = self.cursor.fetchall()
        return lines[0]
