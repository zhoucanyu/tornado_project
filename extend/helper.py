def byte_to_dict(_byte):
    return eval(bytes.decode(_byte))

# 递归合并字典
def deepSearch(dict1,dict2):
	for k in dict2:
		if(type(dict2[k])==type(dict()) and  k in dict1.keys()):
			dict1 [k] = deepSearch(dict1[k],dict2[k])	
		else:
			dict1[k] = dict2[k]
	return dict1
