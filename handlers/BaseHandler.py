from tornado.web import RequestHandler


class BaseHandler(RequestHandler):
    ''' handler 基类 '''
    @property
    def mysql(self):
        return self.application.mysql

    @property
    def redis(self):
        return self.application.redis

    def prepare(self):
        pass

    def write_error(self, status_code, **kwargs):
        pass

    def set_default_headers(self):
        pass

    def initialize(self):
        pass

    def on_finish(self):
        pass
