
from tornado import ioloop, options, httpserver
from config import default, init_config
from app import App
from extend import helper



def main():
    print(default['log_file'])
    options.options.logging = 'warning'
    options.options.log_file_prefix = default['log_file']
    # 项目开启的端口号
    options.define(
        'port',
        default=default['port'],
        type=int,
        help='run server on the given port')

    # 项目名称
    options.define(
        'title',
        default=default['title'],
        type=str,
        help='write your title')

    # 环境（开发 or 生产)
    options.define(
        'environ',
        default='dev',
        type=str,
        help='run server on the given environ')

    # 获取CMD 控制台 传入的参数
    options.parse_command_line()
    # 根据 环境 选择 配置参数
    ops = init_config(options.options.environ)
    # 开启一个 http 服务端
    http_server = httpserver.HTTPServer(App(ops))
    # 监听 端口
    http_server.listen(options.options.port)

    # http_server.bind(options.options.port)

    if(options.options.environ == 'dev'):
        print('Server is running at http://127.0.0.1:%s  title:%s' % (
            options.options.port,
            options.options.title,
        ))
        print('Quit the server with Control+C')
    else:
        print('Server Start')
    ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
