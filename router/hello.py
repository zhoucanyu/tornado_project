from handlers import handler
import re


def get_urls(path=""):
    urls = [
        [path + r"/hello", handler.Hello],
        [path + r"/(?P<id>.+)",  handler.Index],
       # [path + r'/(?P<subject>.+)', handler.Index],
        [path + r"/login", handler.Login],
    ]

    return urls
